﻿/*
SQLyog Community Edition- MySQL GUI v8.12 
MySQL - 5.5.14 : Database - autoray
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`autoray` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `autoray`;

/*Table structure for table `class` */

DROP TABLE IF EXISTS `class`;

CREATE TABLE `class` (
  `ClassId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ClassName` VARCHAR(255) DEFAULT NULL,
  `RootId_FK` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`ClassId`),
  UNIQUE KEY `ClassId_UNIQUE` (`ClassId`),
  UNIQUE KEY `Name_UNIQUE` (`ClassName`),
  KEY `fk_Classes_RootElement` (`RootId_FK`),
  CONSTRAINT `fk_Classes_RootElement` FOREIGN KEY (`RootId_FK`) REFERENCES `rootelement` (`RootId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `class` */

INSERT  INTO `class`(`ClassId`,`ClassName`,`RootId_FK`) VALUES (4,'Ð³Ð»ÑƒÑˆÐ¸Ñ‚ÐµÐ»ÑŒ',6),(5,'Ð¿Ð¾Ð´ÑˆÐ¸Ð±Ð½Ð¸Ðº',7),(6,'ÑÐ°Ð»ÑŒÐ½Ð¸Ðº',7),(7,'ÐºÑ€Ñ‹Ð»Ð¾',8),(9,'Ð±Ð°Ð¼Ð¿ÐµÑ€ Ð¿ÐµÑ€ÐµÐ´Ð½Ð¸Ð¹',8),(10,'Ñ‚Ð¾Ñ€Ð¼Ð¾Ð·Ð½Ñ‹Ðµ Ð´Ð¸ÑÐºÐ¸',11);

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `RoleId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `RoleName` VARCHAR(45) DEFAULT NULL,
  `AccessLevel` INT(11) DEFAULT NULL,
  PRIMARY KEY (`RoleId`),
  UNIQUE KEY `RoleId_UNIQUE` (`RoleId`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Data for the table `role` */

INSERT INTO `role`(`RoleId`,`RoleName`,`AccessLevel`) VALUES (1,'ÐÐ´Ð¼Ð¸Ð½Ð¸ÑÑ‚Ñ€Ð°Ñ‚Ð¾Ñ€',4);
INSERT INTO `role` (`RoleId`,`RoleName`, `AccessLevel`) VALUES (2, 'ÐšÐ»Ð¸ÐµÐ½Ñ‚', 1);
INSERT INTO `role` (`RoleId`, `RoleName`, `AccessLevel`) VALUES (3, 'ÐÐ´Ð¼Ð¸Ð½Ð¸ÑÑ‚Ñ€Ð°Ñ‚Ð¾Ñ€ ÑÑ‚Ð¾', 3);
INSERT INTO `role` (`RoleId`, `RoleName`, `AccessLevel`) VALUES (4, 'Ð Ð°Ð±Ð¾Ñ‚Ð½Ð¸Ðº ÑÑ‚Ð¾', 2);



/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `ClientId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `FirstName` VARCHAR(55) NOT NULL,
  `LastName` VARCHAR(55) NOT NULL,
  `Patronymic` VARCHAR(50) DEFAULT NULL,
  `PassportNumber` VARCHAR(20) NOT NULL,
  `Address` VARCHAR(100) DEFAULT NULL,
  `Telephone` VARCHAR(45) DEFAULT NULL,
  `Email` VARCHAR(45) NOT NULL,
  `bithday` TIMESTAMP,
  `Password` VARCHAR(45) DEFAULT NULL,
  `RoleId_FK` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`ClientId`),
  UNIQUE KEY `ClientId_UNIQUE` (`ClientId`),
  UNIQUE KEY `Email_UNIQUE` (`Email`),
  KEY `fk_Client_Role1` (`RoleId_FK`),
  CONSTRAINT `fk_Client_Role1` FOREIGN KEY (`RoleId_FK`) REFERENCES `role` (`RoleId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Data for the table `users` password - admin*/

INSERT  INTO `users`(`ClientId`,`LastName`,`PassportNumber`,`Address`,`Telephone`,`Email`,`Password`,`RoleId_FK`) VALUES (1,'TEST','1','TEST','23','1@1.ru','21232f297a57a5a743894a0e4a801fc3',1);
INSERT  INTO `users`(`ClientId`,`FirstName`,`LastName`,`Patronymic`,`PassportNumber`,`Address`,`Telephone`,`Email`,`bithday`,`Password`,`RoleId_FK`) VALUES (2,'Masetr','master',NULL,'2','terst addr','45122','1@2.ru','2014-05-30 16:52:15','21232f297a57a5a743894a0e4a801fc3',4);

/*Table structure for table `clientorder` */

DROP TABLE IF EXISTS `clientorder`;

CREATE TABLE `clientorder` (
  `ClientOrderId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `FlagBuying` INT(11) DEFAULT NULL,
  `DateBuying` TIMESTAMP NULL DEFAULT NULL,
  `StationService` BOOLEAN DEFAULT FALSE NOT NULL,
  `ClientId_FK` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`ClientOrderId`),
  UNIQUE KEY `ClientOrderId_UNIQUE` (`ClientOrderId`),
  KEY `fk_ClientOrder_Client1` (`ClientId_FK`),
  CONSTRAINT `fk_ClientOrder_Client1` FOREIGN KEY (`ClientId_FK`) REFERENCES `users` (`ClientId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Data for the table `clientorder` */

INSERT  INTO `clientorder`(`ClientOrderId`,`FlagBuying`,`DateBuying`,`ClientId_FK`) VALUES (1,0,'0000-00-00 00:00:00',1),(2,1,NULL,1),(6,1,NULL,1),(7,1,NULL,1),(8,1,NULL,1),(9,0,NULL,1),(10,1,NULL,1),(11,0,NULL,1),(12,1,NULL,1),(13,1,NULL,1),(14,1,NULL,1),(15,1,NULL,1),(16,1,'2013-09-16 21:46:37',1),(19,0,NULL,1);

/*Table structure for table `firm` */

DROP TABLE IF EXISTS `firm`;

CREATE TABLE `firm` (
  `FirmId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `FirmName` VARCHAR(255) NOT NULL,
  `Country` VARCHAR(255) DEFAULT NULL,
  `City` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`FirmId`),
  UNIQUE KEY `FirmId_UNIQUE` (`FirmId`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Data for the table `firm` */

INSERT  INTO `firm`(`FirmId`,`FirmName`,`Country`,`City`) VALUES (2,'By Car','Ð“ÐµÑ€Ð¼Ð°Ð½Ð¸Ñ','Ð‘ÐµÑ€Ð»Ð¸Ð½'),(5,'Drive your dream','Ð¤Ñ€Ð°Ð½Ñ†Ð¸Ñ','Ð‘Ñ€ÐµÑÑ‚'),(6,'Mobil','ÐŸÐ¾Ð»ÑŒÑˆÐ°','Ð“Ð´Ð°Ð½ÑŒÑÐº');

/*Table structure for table `mark` */

DROP TABLE IF EXISTS `mark`;

CREATE TABLE `mark` (
  `MarkId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `MarkName` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`MarkId`),
  UNIQUE KEY `idMark_UNIQUE` (`MarkId`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Data for the table `mark` */

INSERT  INTO `mark`(`MarkId`,`MarkName`) VALUES (1,'Ford'),(2,'Audi'),(9,'Honda'),(10,'Toyota');

/*Table structure for table `model` */

DROP TABLE IF EXISTS `model`;

CREATE TABLE `model` (
  `ModelId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ModelName` VARCHAR(255) NOT NULL,
  `Year` YEAR(4) DEFAULT NULL,
  `MarkId_FK` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`ModelId`),
  UNIQUE KEY `ModelsId_UNIQUE` (`ModelId`),
  KEY `fk_Model_Mark1` (`MarkId_FK`),
  CONSTRAINT `fk_Model_Mark1` FOREIGN KEY (`MarkId_FK`) REFERENCES `mark` (`MarkId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Data for the table `model` */

INSERT  INTO `model`(`ModelId`,`ModelName`,`Year`,`MarkId_FK`) VALUES (1,'Fiesta',1990,1),(2,'A4',2009,2),(5,'avensis',1999,10),(6,'accord',2005,9),(7,'s8',2002,2),(8,'c-max',2002,1);

/*Table structure for table `model_has_warehouse` */

DROP TABLE IF EXISTS `model_has_warehouse`;

CREATE TABLE `model_has_warehouse` (
  `ModelId_FK` INT(10) UNSIGNED NOT NULL,
  `WarehouseId_FK` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`ModelId_FK`,`WarehouseId_FK`),
  KEY `fk_Model_has_Warehouse_Warehouse1` (`WarehouseId_FK`),
  CONSTRAINT `fk_Model_has_Warehouse_Model1` FOREIGN KEY (`ModelId_FK`) REFERENCES `model` (`ModelId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_Model_has_Warehouse_Warehouse1` FOREIGN KEY (`WarehouseId_FK`) REFERENCES `warehouse` (`WarehouseId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=INNODB DEFAULT CHARSET=utf8;

/*Data for the table `model_has_warehouse` */

INSERT  INTO `model_has_warehouse`(`ModelId_FK`,`WarehouseId_FK`) VALUES (2,4);

/*Table structure for table `ordercontent` */

DROP TABLE IF EXISTS `ordercontent`;

CREATE TABLE `ordercontent` (
  `OrderContentId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `DetailCount` INT(11) DEFAULT NULL,
  `WarehouseId_FK` INT(10) UNSIGNED NOT NULL,
  `ClientOrderId_FK` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`OrderContentId`),
  UNIQUE KEY `OrderContentId_UNIQUE` (`OrderContentId`),
  KEY `fk_OrderContent_ClientOrder2` (`ClientOrderId_FK`),
  CONSTRAINT `fk_OrderContent_ClientOrder1` FOREIGN KEY (`ClientOrderId_FK`) REFERENCES `clientorder` (`ClientOrderId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_OrderContent_ClientOrder2` FOREIGN KEY (`ClientOrderId_FK`) REFERENCES `clientorder` (`ClientOrderId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Data for the table `ordercontent` */

INSERT  INTO `ordercontent`(`OrderContentId`,`DetailCount`,`WarehouseId_FK`,`ClientOrderId_FK`) VALUES (3,1,2,7),(4,1,2,7),(10,1,2,8),(11,1,2,8),(13,1,2,8),(14,1,2,8),(15,1,3,10),(26,1,3,11),(35,1,3,14),(38,1,3,15),(39,1,2,15),(40,1,2,16),(43,1,4,19);

/*Table structure for table `rootelement` */

DROP TABLE IF EXISTS `rootelement`;

CREATE TABLE `rootelement` (
  `RootId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `RootName` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`RootId`),
  UNIQUE KEY `RootId_UNIQUE` (`RootId`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Data for the table `rootelement` */

INSERT  INTO `rootelement`(`RootId`,`RootName`) VALUES (6,'Ð²Ñ‹Ñ…Ð»Ð¾Ð¿Ð½Ð°Ñ ÑÐ¸ÑÑ‚ÐµÐ¼Ð°'),(7,'Ñ…Ð¾Ð´Ð¾Ð²Ð°Ñ Ñ‡Ð°ÑÑ‚ÑŒ'),(8,'ÐºÑƒÐ·Ð¾Ð²'),(9,'Ð¿Ð¾Ð´Ð²ÐµÑÐºÐ°'),(10,'Ð´Ð²Ð¸Ð³Ð°Ñ‚ÐµÐ»ÑŒ'),(11,'Ñ‚Ð¾Ñ€Ð¼Ð¾Ð·Ð½Ð°Ñ ÑÐ¸ÑÑ‚ÐµÐ¼Ð°');

/*Table structure for table `warehouse` */

DROP TABLE IF EXISTS `warehouse`;

CREATE TABLE `warehouse` (
  `WarehouseId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `DetailNumber` VARCHAR(45) NOT NULL,
  `Price` INT(11) DEFAULT NULL,
  `Count` INT(11) NOT NULL,
  `FirmId_FK` INT(10) UNSIGNED NOT NULL,
  `DetailName` VARCHAR(255) NOT NULL,
  `Year` YEAR(4) NOT NULL,
  `ClassId_FK` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`WarehouseId`),
  UNIQUE KEY `WarehouseId_UNIQUE` (`WarehouseId`),
  UNIQUE KEY `DetailNumber_UNIQUE` (`DetailNumber`),
  KEY `fk_Warehouse_Firm1` (`FirmId_FK`),
  KEY `fk_Warehouse_Class1` (`ClassId_FK`),
  CONSTRAINT `fk_Warehouse_Class1` FOREIGN KEY (`ClassId_FK`) REFERENCES `class` (`ClassId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_Warehouse_Firm1` FOREIGN KEY (`FirmId_FK`) REFERENCES `firm` (`FirmId`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

/*Data for the table `warehouse` */

INSERT  INTO `warehouse`(`WarehouseId`,`DetailNumber`,`Price`,`Count`,`FirmId_FK`,`DetailName`,`Year`,`ClassId_FK`) VALUES (4,'qw1234',230,15,5,'Ð±Ð°Ð¼Ð¿ÐµÑ€',2013,9);

CREATE TABLE `auto` (
  `Id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Model_FK` INT(10) UNSIGNED NOT NULL,
  `Users_FK` INT(10) UNSIGNED NOT NULL,
  `Year` INT(10) UNSIGNED NOT NULL,
  `Engine` VARCHAR(20) NOT NULL,
  `CarBody` VARCHAR(30) NOT NULL,
  `Number` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Number` (`Number`(20)),
  KEY `fk_Model_Client` (`Model_FK`),
  KEY `fk_Users_Client` (`Users_FK`),
  CONSTRAINT `fk_Model_Client` FOREIGN KEY (`Model_FK`) REFERENCES `model` (`ModelId`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_Users_Client` FOREIGN KEY (`Users_FK`) REFERENCES `users` (`ClientId`) ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT  INTO `auto`(`Id`,`Model_FK`,`Users_FK`,`Year`,`Engine`,`CarBody`,`Number`) VALUES (1,2,1,2014,'2,2 Ð´Ð¸Ð·ÐµÐ»ÑŒ','Ð¡ÐµÐ´Ð°Ð½','1456 ix-4');

CREATE TABLE `work_status` (
  `Id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Code` VARCHAR(10) NOT NULL,
  `Description` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `work_status` (`Code`, `Description`) VALUES ('IN_WORK', "Ð’ Ñ€Ð°Ð±Ð¾Ñ‚Ðµ"), ('DONE', "Ð—Ð°ÐºÐ¾Ð½Ñ‡ÐµÐ½Ð¾"), ('IN_QUEUE', "Ð’ Ð¾Ñ‡ÐµÑ€ÐµÐ´Ð¸");

CREATE TABLE `station_order` (
  `Id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Auto_FK` INT(10) UNSIGNED NOT NULL,
  `StartDate` TIMESTAMP NULL DEFAULT NULL,
  `EndDate` TIMESTAMP NULL DEFAULT NULL,
  `WorkDescr` VARCHAR(200) ,
  `Price` INT(10),
  `Status_Id` INT(10) UNSIGNED NOT NULL,
  `Master_Id` INT(10) UNSIGNED NOT NULL,
  `ClientOrderId_FK` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_Auto_Station_Oreder` (`Auto_FK`),
  KEY `fk_WStatus_Station_Order` (`Status_Id`),
  KEY `fk_Master_Station_Order` (`Master_Id`),
  KEY `fk_Clientorder_Station_Order` (`ClientOrderId_FK`),
  CONSTRAINT `fk_Auto_Station_Order` FOREIGN KEY (`Auto_FK`) REFERENCES `auto` (`Id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_WStatus_Station_Order` FOREIGN KEY (`Status_Id`) REFERENCES `work_status` (`Id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_Master_Station_Order` FOREIGN KEY (`Master_Id`) REFERENCES `users` (`ClientId`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_Clientorder_Station_Order` FOREIGN KEY (`ClientOrderId_FK`) REFERENCES `clientorder` (`ClientOrderId`) ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT  INTO `station_order`(`Id`,`Auto_FK`,`StartDate`,`EndDate`,`WorkDescr`,`Price`,`Status_Id`,`Master_Id`,`ClientOrderId_FK`) VALUES (3,1,'2012-05-20 14:00:00','2014-06-20 14:00:00',"Ð’Ð°Ñ‚Ð°",3000,1,2, 19);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
