﻿<?php
  $error['mysql_connect_error'] = 'Ошибка подключения к бызе данных. Обратитесь к системному администратору.';
  $error['passboteq'] = "Пароли не совпадают. Пожалуйста, повторите еще раз.";
  $error['failed'] = "Ошибка при выполнении операции.";
  $error['req'] = "Заполните все обязательные поля.";
  $error['permissions'] = "У вас нет прав доступа к данной странице. Обратитесь к системному администратору.";
  $error['count'] = "Недостаточно деталей на складе.";
  $error['foreign'] = "Удаление невозможно. Обноружена связанная запись.";

  $info['trytologin'] = "Теперь вы можете войти в систему.";
  $info['signin'] = "Вход выполнен успешно.";
  $info['alreadysignin'] = "Вы уже авторизированы.";
  $info['ok'] = "Операция произведена успешно.";
  
  /*$error['mysql_connect_error'] = $prefix.'Cannot connect to the database. Please, contact to your system administrator.'.$postfix;
  $error['passboteq'] = $prefix."Passwords are not equal. Retype it and try to submit once again.".$postfix;
  $error['failed'] = $prefix."The operation you try to make is failed.".$postfix;
  $error['req'] = $prefix."Fill all required fields.".$postfix;
  $error['permissions'] = $prefix."You don't have permission to access this page. Please contact your system administrator.".$postfix;
  $error['count'] = $prefix."There are not enought details in the warehouse.".$postfix;

  $info['trytologin'] = "If everything correct try to log in yourself.";
  $info['signin'] = "You signed in successfully.";
  $info['alreadysignin'] = "You are already signed in.";
  $info['ok'] = "The operation success.";*/
?>