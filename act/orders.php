<?php
  require_once("db.php");
  require_once("security.php");

  function init() {
	if (permission(USER)) {
	  	$query = sprintf("INSERT INTO ClientOrder (FlagBuying, ClientId_FK) VALUES (0, %d)", $_SESSION['info']['id']);
	  	mysql_query($query);
	  	$_SESSION['OrderId'] = mysql_insert_id();
		}
	}

  function buy($id,$count, $cart) {  	if (permission(USER)) {		$query = sprintf("INSERT INTO OrderContent (DetailCount, WarehouseId_FK, ClientOrderId_FK) VALUES (%d, %d, %d)", $count, $id, $cart);
		mysql_query($query);
		if (!mysql_error()) return true;
	}
	return false;
  }

  function removeOrderContent($id) {  	if (permission(USER)) {	    $query = sprintf("DELETE FROM OrderContent WHERE OrderContentId = %d AND ClientOrderId_FK = %d", $id, $_SESSION['OrderId']);
		mysql_query($query);
	}
  }

  function removeClinetOrder() {
  	if (permission(USER)) {		$query = sprintf("DELETE FROM ClientOrder WHERE FlagBuying = 0 AND ClientOrderId = %d", $_SESSION['OrderId']);
		mysql_query($query);
	}
  }

  function orderSubmit() {  	if (permission(USER)) {	  	$rollback = false;		mysql_query("START TRANSACTION");
	    $query = sprintf("UPDATE Warehouse INNER JOIN ".
	    				"(SELECT *, SUM(DetailCount) Total FROM OrderContent WHERE ClientOrderId_FK = %d GROUP BY WarehouseId_FK) COrder ".
						"ON WarehouseId = WarehouseId_FK SET Count = Count - Total", $_SESSION['OrderId']);

		mysql_query($query);
		if (mysql_error()) $rollback = true;

		$query = sprintf("UPDATE ClientOrder SET FlagBuying=1, DateBuying=now() WHERE ClientOrderId= %d",$_SESSION['OrderId']);
	    mysql_query($query);

		if (mysql_error() || $rollback) {			mysql_query("ROLLBACK");
			return false;
		}
		else {			mysql_query("COMMIT");
			init();
			return true;
		}
	}
  }
?>