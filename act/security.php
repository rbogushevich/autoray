
<?php
  require_once("messages.php");
  DEFINE("USER", 1);
  DEFINE("STATION_EMPLOYEE", 2);
  DEFINE("STATION_ADMIN", 3);
  DEFINE("ADMIN", 4);
  
  function permissionWithError($level, $showMessage) {
	if (!isset($_SESSION['info']['AccessLevel']) || $_SESSION['info']['AccessLevel'] < $level) {		global $error;
	   	if ($showMessage) 
			echo($error['permissions']);
	   	return false;
	 }
	 return true;
  }
  
  function permission($level) {
	return permissionWithError($level, true);
  }
?>