<?php

	$typeParamName = "type";
	$classParamName = "class";
	$showsubdir = "act/showtypes/";

	if (isset($_REQUEST[$typeParamName]) && ctype_alnum($_REQUEST[$typeParamName])
									&& file_exists($showsubdir.$_REQUEST[$typeParamName].".php"))
		include($showsubdir.$_REQUEST[$typeParamName].".php");
?>