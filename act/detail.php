﻿<?php require_once($subdir."db.php");
	  require_once($subdir."messages.php");
	  require_once($subdir."security.php");
	  if (permission(ADMIN)) {
?>

<form action="" method="post">
<table>
<tr>
	<td>
    	Подтип
	</td>
	<td>
		<select size="1" name="class">
			<?php
	           $query = sprintf("SELECT * FROM Class");
	           $result = mysql_query($query);
	           while ($row = mysql_fetch_array($result))
	           		echo("<option value=\"".$row['ClassId']."\">".$row['ClassName']."</option>");

			?>
		</select>
	</td>
</tr>
<tr>
	<td>
		Фирма
	</td>
	<td>
		<select size="1" name="firm">
			<?php
	           $query = sprintf("SELECT * FROM Firm");
	           $result = mysql_query($query);
	           while ($row = mysql_fetch_array($result))
	           		echo("<option value=\"".$row['FirmId']."\">".$row['FirmName']."</option>");
			?>
		</select>
	</td>
</tr>
<tr>
	<td>
		Модель
	</td>
	<td>
		<select size="5" name="model[]" multiple="multiple">
			<?php
	           $query = sprintf("SELECT * FROM Model");
	           $result = mysql_query($query);
	           while ($row = mysql_fetch_array($result))
	           		echo("<option value=\"".$row['ModelId']."\">".$row['ModelName']."</option>");

			?>
		</select>
	</td>
</tr>
<tr>
	<td>
		Название
	</td>
	<td>
		<input name="detailname" type="text">
	</td>
</tr>
<tr>
	<td>
		Номер
	</td>
	<td>
		<input name="detailnumber" type="text">
	</td>
</tr>
<tr>
	<td>
		Цена
	</td>
	<td>
		<input name="price" type="text">
	</td>
</tr>
<tr>
	<td>
		Количество
	</td>
	<td>
		<input name="count" type="text">
	</td>
</tr>
<tr>
    <td>
    	Год
    </td>
    <td>
		<input name="year" type="text">
	</td>
</tr>
	<input name="op" type="hidden" value="make">
<tr>
	<td>
		<input type="submit" value="Сохранить">
	</td>
</tr>
	<?php
		if (isset($_REQUEST['op'])) {
		    if (!(empty($_REQUEST['class']) ||
		    	empty($_REQUEST['firm']) ||
		    	empty($_REQUEST['model']) ||
		    	empty($_REQUEST['detailname']) ||
		    	empty($_REQUEST['detailnumber']) ||
		    	empty($_REQUEST['price']) ||
		    	empty($_REQUEST['count']) ||
		    	empty($_REQUEST['year']))) {
	                 $query = sprintf("INSERT INTO Warehouse (DetailNumber, Price, Count, FirmId_FK, DetailName, Year, ClassId_FK) ".
	                 					"VALUES ('%s', %s, %s, %s, '%s', %s, %s)",
		    							$_REQUEST['detailnumber'],
		    							$_REQUEST['price'],
		    							$_REQUEST['count'],
		    							$_REQUEST['firm'],
		    							$_REQUEST['detailname'],
		    							$_REQUEST['year'],
		    							$_REQUEST['class']);
		    		 mysql_query($query);

	                 if (mysql_error())
	                 	echo(mysql_error()." ".$error['failed']);
	                 $id = mysql_insert_id();
	              	 foreach ($_REQUEST['model'] as $key => $value) {
		                 $query = sprintf("INSERT INTO Model_has_Warehouse (ModelId_FK, WarehouseId_FK) ".
		                 					"VALUES (%d, $id)", $value);
						 mysql_query($query);
		             }
	        }
		  	else
		  		echo($error['req']);
		}
}	?>
</table>
</form>