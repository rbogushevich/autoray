<?php
	require_once($subdir."db.php");
	require_once($subdir."security.php");

	function addClass($name, $rootId) {
		if (permission(ADMIN)) {
		  	$query = sprintf("INSERT INTO Class (ClassName, RootId_FK) VALUES ('%s', %d)", $name, $rootId);
		  	mysql_query($query);
		  	return mysql_insert_id();
		}
	}

	function deleteClass($id) {
		if (permission(ADMIN)) {
		  	$query = sprintf("DELETE FROM Class WHERE ClassId=%d", $id);
		  	mysql_query($query);
		}
	}

	function addRoot($name) {
		if (permission(ADMIN)) {
		  	$query = sprintf("INSERT INTO Rootelement (RootName) VALUES ('%s')", $name);
		  	mysql_query($query);
		  	return mysql_insert_id();
		}
	}

	function deleteRoot($id) {
		if (permission(ADMIN)) {
			$query = sprintf("DELETE FROM Rootelement WHERE RootId=%d", $id);
		  	mysql_query($query);
		}
	}

	function addFirm($name, $country, $city) {
		if (permission(ADMIN)) {
	 		$query = sprintf("INSERT INTO Firm (FirmName, Country, City) ".
	 						 "VALUES ('%s', '%s', '%s');", $name, $country, $city);
		  	mysql_query($query);
		  	return mysql_insert_id();
		}
	}

	function deleteFirm($id) {
		if (permission(ADMIN)) {
	    	$query = sprintf("DELETE FROM Firm WHERE FirmId=%d", $id);
		  	mysql_query($query);
		}
	}

	function addMark($name) {
		if (permission(ADMIN)) {
			$query = sprintf("INSERT INTO Mark (MarkName) VALUES ('%s')", $name);
		  	mysql_query($query);
		  	return mysql_insert_id();
		}
	}

	function deleteMark($id) {
		if (permission(ADMIN)) {
	        $query = sprintf("DELETE FROM Mark WHERE MarkId=%d", $id);
		  	mysql_query($query);
		}
	}

	function addModel($name, $year, $markId) {
		if (permission(ADMIN)) {
	        $query = sprintf("INSERT INTO Model (ModelName, Year, MarkId_FK) ".
	        				 "VALUES ('%s', %d, %d)", $name, $year, $markId);
		  	mysql_query($query);
		  	return mysql_insert_id();
		}
	}

	function deleteModel($id) {
		if (permission(ADMIN)) {
			$query = sprintf("DELETE FROM Model WHERE ModelId=%d", $id);
		  	mysql_query($query);
		}
	}
	
	function addAuto($Model_FK, $Users_FK, $Year, $Engine, $CarBody,$Number) {
		if (permission(STATION_ADMIN)) {
	        $query = sprintf("INSERT INTO auto (Model_FK, Users_FK, Year,Engine,CarBody,Number) ".
	        				 "VALUES (%d, %d, %d,'%s','%s','%s')", $Model_FK, $Users_FK, $Year, $Engine, $CarBody,$Number);
		  	mysql_query($query);
		  	return mysql_insert_id();
		}
	}

	function deleteAuto($id) {
		if (permission(ADMIN)) {
			$query = sprintf("DELETE FROM Auto WHERE Id=%d", $id);
		  	return mysql_query($query);
		}
	}
	
	function addUser($FirstName, $LastName, $Patronymic, $PassportNumber, $Address,$Telephone, $Email,$bithday, $Password, $RoleId_FK) {
		if (permission(ADMIN)) {
	        $query = sprintf("INSERT INTO users (FirstName, LastName, Patronymic,PassportNumber,Address,Telephone,Email,bithday,Password,RoleId_FK) ".
	        				 "VALUES ('%s', '%s', '%s','%s','%s','%s','%s','%s','%s',%d)", $FirstName, $LastName, $Patronymic, $PassportNumber, $Address,$Telephone, $Email,$bithday, md5($Password), $RoleId_FK);
			mysql_query($query);				 
		  	return mysql_insert_id();
		}
	}

	function deleteUser($id) {
		if (permission(ADMIN)) {
			$query = sprintf("DELETE FROM Users WHERE ClientId=%d", $id);
		  	mysql_query($query);
			mysql_error(); 
		}
	}
	
	function addServiceOrder($Auto_FK, $StartDate, $EndDate,$WorkDescr,$Price,$Status_Id,$Master_Id,$ClientOrderId_FK) {
	if (permission(STATION_ADMIN)) {
	        $query = sprintf("INSERT INTO station_order (Auto_FK, StartDate, EndDate,WorkDescr,Price,Status_Id,Master_Id,ClientOrderId_FK) ".
	        				 "VALUES (%d, '%s', '%s','%s',%d,%d,%d,%d)", $Auto_FK, $StartDate, $EndDate,$WorkDescr,$Price,$Status_Id,$Master_Id,$ClientOrderId_FK);
							 
			mysql_query($query);				 
			//$f = fopen("d:\log.txt", "w");


		//fwrite($f , mysql_error()); 
		//fwrite($f , $query); 
			//fclose($f);
		  	return mysql_insert_id();
		}
	}
	
	function toNextStep($id, $status_id) {
		$query = sprintf("UPDATE station_order AS so SET  so.Status_id = %d WHERE so.id = %d", $status_id, $id);
		mysql_query($query);
	}
?>