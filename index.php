<?php session_start();
	require_once('act/messages.php');
	require_once('act/db.php');
	require_once('act/orders.php');
	require_once("act/security.php");
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>АвтоРай</title>
<meta name="description" content="AutoRay">
<meta name="keywords" content="autos, cars, dealer">
		<script src="js/jquery-1.7.js"></script>
		<script src="js/firm.js"></script>
		<script src="js/hierarchy.js"></script>
		<link rel=StyleSheet href="css/style.css" type="text/css">
		<link rel=StyleSheet href="css/hierarchy.css" type="text/css">
		<link rel=StyleSheet href="css/firm.css" type="text/css">

</head>
<body>
<?php
    require_once("act/security.php");
	if (permissionWithError(STATION_ADMIN, false)) {
?>
<div id="settings">
<table>
<?php
	if (permissionWithError(ADMIN, false)) {
?>
	<tr>
		<td><a href="?do=root">Редактировать тип</a></td>
		<td><a href="?do=model">Редактировать модель</a></td>
		<td><a href="?do=deldet">Редактировать запчасть</a></td>
	</tr>
	<tr>
		<td><a href="?do=class">Редактировать подтип</a></td>
		<td><a href="?do=mark">Редактировать марку</a></td>
		<td><a href="?do=clientorders">Показать заказы клиента</a></td>
  	</tr>
  	<tr>
        <td><a href="?do=firm">Редактировать список фирм</a></td>
        <td><a href="?do=detail">Добавиь новую деталь</a></td>
        <td></td>
	</tr>
	<?php
	}
?>
	<tr>
        <td><a href="?do=serviceorders">Заказы СТО</a></td>
        <td><a href="?do=auto">Автомобили</a></td>
        <td><a href="?do=users">Пользователи</a></td>
	</tr>
</table>
</div>
<?php } ?>
<?php
  if (isset($_SESSION['info']['name'])) {
?>
<div id="userdiv">

	<?php 
	
	echo($_SESSION['info']['name']."(".$_SESSION['info']['RoleName'].")") ?> &nbsp;
    [<a href="logout.php">Выход</a>] &nbsp;
    <a href="?do=cart"><img src="images/cart.png"></a>
</div>
<?php } ?>
	<script>
		$("body").css('opacity','0.0');
		var dtime = 500;
		var url = '/';

		function reloadit(url) {
                	$("body").animate({
                			opacity : 0
                		},  dtime).queue(function(){
                						document.location = url;
                				   });
                	return false;
   		}

		$(document).ready(function() {
			$("a[href]").not('.reallink').bind({
				click: function() {
					return reloadit(this.href);
				}
			});
			if ($("#settings").length) {
				$("#settings").click(function() {
	  				$("#settings").animate({top : 0}, { duration:600 }, 'swing');
				});
				$("#page").click(function() {
	  				$("#settings").animate({top : -95}, { duration:600 }, 'swing');
				});
			}
			$("body").animate({opacity : 1.0}, { duration:800 }, 'swing');
		});
	</script>
<div id="page">
<div id="pagewrap">
<div id="sidebar">
	<div id="logowrap">
    <div id="logo"><img src="images/logo.jpg" alt=""></div>
    <div id="company_name">АвтоРай</div><div id="company_name_shadow">АвтоРай</div>
    </div>
<br/>
    <div id="menuwrap"><img src="images/menu_top.gif" alt="">
    <ul id="menu">
    	<li><a href=".">Главная</a></li>
<?php
	if (!isset($_SESSION['info'])) { ?>
        <li><a href="?do=reg">Регистрация</a></li>
        <li><a href="?do=login">Войти</a></li>
<?php } ?>
    	<li><a href="?do=show&type=byhierarchy">Поиск по типу</a></li>
    	<li><a href="?do=show&type=byfirm">Поиск по фирме</a></li>
    	<li><a href="?do=show&type=bymark">Поиск по марке</a></li>
    	<li><a href="?do=show&type=bydetnum">Поиск по номеру детали</a></li>
    </ul>
    <div class="bottom"><img src="images/menu_bottom.gif" alt="" width="250" height="10"></div>

    </div>
    <div id="news">
    <h2> <?php include 'contact.txt'; ?> </h2>
    </div>
</div>

<div id="contentwrap"><div id="header"></div>
<div id="body_txt">
		<?php
		  $param = "do";
		  $subdir = "act/";

		  if (isset($_REQUEST[$param]) && ctype_alnum($_REQUEST[$param]) && file_exists($subdir.$_REQUEST[$param].".php"))
		      	include($subdir.$_REQUEST[$param].".php");
		  else {
				?>

<!-- !!!!!!!!!!!!!!!!!!!!Вставь свой текст сюда!!!!!!!!!!!!!!!!!!!!!-->
		<?php
			 }
		?>
	</div>
</div>
</div>
<div id="footer">
  <div id="bottom_menu"><a href="/warehouse">Home</a>  |  <a href="?do=reg">Registration</a> </div>
</div>
</div>

</body>
</html>
