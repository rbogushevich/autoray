DELIMITER |
CREATE TRIGGER `DateDefaultValue` before insert on `Clientorder`
For each row begin
    SET NEW.DateBuying = NOW();
END |